
<table id="tasks" class="display table table-hover" data-page-length='3'>
	 <thead>
		  <tr>
			   <th>№ задачи</th>
			   <th>Пользователь</th>
			   <th>E-mail</th>
			   <th>Описание</th>
			   <th>Статус</th>
		  </tr>
	 </thead>
	 <tbody>
		 <?php foreach ($data as $item): ?>
			 <?php
			 $status = $item->getStatus();
			 $class = 'table-light';
			 switch ($status) {
				 case 0:
					 $sostoyanie = "В работе!";
					 $class = 'clicable table-secondary';
					 break;
				 case 1:
					 $sostoyanie = "Выполнено!";
					 $isAdminEdit = $item->getEditedByAdmin();
					 if($isAdminEdit){
						 $sostoyanie .= " Отредактировано администратором";
					 }
					 $class = 'clicable table-success';
					 if ($_SESSION['role'] == 'admin') {
						 $class = 'clicable table-success';
					 }
					 break;
			 }
			 ?>
			  <tr class="<?php echo $class ?>" data-id="<?php echo $item->getid() ?>">
				   <td>
					   <?php echo $item->getId(); ?>
				   </td>
				   <td>
					   <?php echo $item->getUser(); ?>
				   </td>
				   <td>
					   <?php echo $item->getEmail(); ?>
				   </td>
				   <td>
					   <?php echo $item->getDescription(); ?>
				   </td>
				   <td>
					   <?php echo $sostoyanie; ?>
				   </td>
			  </tr>
		  <?php endforeach; ?>
	 </tbody>
</table>

<div class="js-info" 
	 data-dataurl="getData" 
	 data-savedataurl="saveData" 
	 data-redirecturl="../task/list"
	 ></div>
<div class="modal fade show" id="modal"  data-keyboard="false" data-backdrop="static" role='dialog'>
	 <div class="modal-dialog">
		  <div class="modal-content">
			   <div class="modal-header">
					<h4 class="modal-title">
						 Задание №<span id="task_id"></span>
					</h4>
					<button type="button" class="close" data-dismiss="modal">&times;</button>
			   </div>
			   <div class="modal-body">
					<div class="row">
						 <div class="col-12">
							  <input type="text" name="name" class="name form-control required" placeholder="Имя">
							  <span class="text-danger d-none">Поле обязательно для заполнения</span>
						 </div>
					</div>
					<div class="row">
						 <div class="col-12">
							  <input type="email" name="email" class="email form-control mb-2 mt-2 required" placeholder="E-mail">
							  <span class="text-danger d-none">Поле обязательно для заполнения</span>
						 </div>
					</div>
					<div class="row">
						 <div class="col-12">
							  <textarea class="description form-control required"  placeholder="Текст...">
							  </textarea>
							  <span class="text-danger d-none">Поле обязательно для заполнения</span>
						 </div>
					</div>
					<div class="row">
						 <div class="col-12" id="status">
							  <label for="status">Статус</label>
							  <input type="checkbox" class='status'>
						 </div>
					</div>
			   </div>
			   <div class="modal-footer">
					<div class="btn-group">
						 <input type="hidden" class='id' value=''>
						 <button type="button" class="btn btn-primary js-save">Сохранить</button>
						 <button type="button" class="btn btn-danger" data-dismiss="modal">Отменить</button>
					</div>
			   </div>
		  </div>
	 </div>
</div>