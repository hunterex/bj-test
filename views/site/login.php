<div class="row justify-content-center no-gutters">
	 <div class="col-auto text-center">
		  <form class="form-signin"  method='post' action = "login">

			   <h1 class="h3 mb-3 mt-4 font-weight-normal">Вход</h1>

			   <input type="text" id="login" class="form-control" placeholder="Логин" autofocus="" name='login' required="" >

			   <input type="password" id="inputPassword" class="mb-3 mt-3 form-control" placeholder="Пароль" required="" name='password'>

			   <button class="btn btn-lg btn-primary btn-block" type="submit">Войти</button>

			   <?php if (!empty($flash)): ?>
				   <span class='text-danger '>Введены неверные данные</span>
			   <?php endif; ?>

			   <p class="mt-5 mb-3 text-muted">©2019</p>
		  </form>
	 </div>
</div>