<?php
session_start();
?>
<!DOCTYPE html>
<html lang="ru">
	 <head>
		  <meta charset="utf-8">
		  <title>Вход</title>
		  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		  <link rel="stylesheet" type="text/css" href="../css/style.css" />
		  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" 
				integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" 
				crossorigin="anonymous">
		  <link href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css" 
				rel="stylesheet"
				crossorigin="anonymous">
		  <link href = "//cdn.datatables.net/responsive/2.2.3/css/dataTables.responsive.css"
				crossorigin="anonymous">
		  <script
			  src="https://code.jquery.com/jquery-3.4.1.min.js"
			  integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
		  crossorigin="anonymous"></script>
		  <script
			  src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"
			  integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU="
		  crossorigin="anonymous"></script>
		  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" 
				  integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" 
		  crossorigin="anonymous"></script>
		  <script
			  src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"
		  crossorigin="anonymous"></script>
		  <script	src="../js/script.js"></script>
	 </head>
	 <body>
		  <nav class="navbar navbar-expand-sm bg-light navbar-light">
			   <ul class="navbar-nav">
					<li class="nav-item">
						 <a href="../task/list" class="nav-link btn btn-default clicable"/>Главная</a>
					</li>
					<?php if (!empty($_SESSION['role']) && $_SESSION['role'] === 'admin'): ?>
						<li class="nav-item active ">
							 <a href="../site/logout" class="nav-link btn btn-default"/>Выход</a>
						</li>
					<?php else: ?>
						<li class="nav-item active">
							 <a href="../site/login" class="nav-link btn btn-default"/>Вход</a>
						</li>
					<?php endif; ?>
			   </ul>
		  </nav>
		  <div class = "container">
			   <?php include 'views/' . $content_view . '.php'; ?>
		  </div>
	 </body>
</html>