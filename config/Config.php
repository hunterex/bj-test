<?php

final class Config {

	private static $DATA = null;

	public static function getConfig($section = null) {
		if ($section === null) {
			return self::getData();
		}
		$data = self::getData();
		if (array_key_exists($section, $data)) {
			return $data[$section];
		}
		return $data;
	}

	private static function getData() {
		if (self::$DATA !== null) {
			return self::$DATA;
		}
		self::$DATA = parse_ini_file('config.ini', true);
		return self::$DATA;
	}

}
