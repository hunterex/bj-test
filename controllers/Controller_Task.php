<?php

use core\Controller;

class Controller_Task extends Controller {

	public function actionIndex() {
		$this->redirect('task/list');
	}

	public function actionList() {
		$this->view->render('task/main_page', 'main', ['data' => Model_Task::getTask()]);
	}

	public function actionGetData() {
		$data = [];
		if (!empty($_POST['id'])) {
			$data = Model_Task::getTask($_POST['id']);
		}
		session_start();
		$data['isAdmin'] = false;
		if (isset($_SESSION['role']) && $_SESSION['role'] === 'admin') {
			$data['isAdmin'] = true;
		}
		if($data['status'] == 1 && !$data['isAdmin']){
			unset($data['name'],$data['email'],$data['description']);
		}
		$this->responseJson($data);
	}

	public function actionSaveData() {
		session_start();
		$res = ['success' => false];
		if (empty($_POST['id'])) {
			$task = new Model_Task();
		} else {
			$task = Model_Task::getTask($_POST['id'], false);
		}
		if ($_POST['status'] == 1 
			&& isset($_SESSION['role']) 
			&& $_SESSION['role'] === 'admin'
			&& $_POST['description'] !== $task->getDescription()) {
				$_POST['edited_by_admin'] = 1;
			} else {
				$_POST['edited_by_admin'] = 0;
			}
		TaskMapper::map($task, $_POST);
		return $this->responseJson(['success' => $task->save()]);
	}

}
