<?php
use core\Controller;

class Controller_Site extends Controller {

	public function actionIndex(){
		$this->redirect('task/list');
	}
	
	public function actionLogin(){
		$flash = '';
		if(!empty($_POST['login']) && !empty($_POST['password'])){
			session_start();
			if($_POST['login'] == 'admin' && $_POST['password'] == '123'){
				$_SESSION['role'] = 'admin';
				return $this->redirect('task/list');
			} else {
				$flash = "Введенные данные не верны";
			}
		}
		$this->view->render('site/login', 'login',['flash' => $flash]);
	}
	public function actionLogout() {
		session_start();
		$_SESSION['role'] = '';
		session_write_close();
		return $this->redirect('task/list');
	}
}