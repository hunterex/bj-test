<?php

namespace app\components\Api;

use JsonSerializable;

class ApiResponse implements JsonSerializable {

	const STATUS_SUCCESS	 = 'success';
	const STATUS_ERROR	 = 'error';

	private $status	 = null;
	private $data	 = null;
	private $error	 = null;

	public function __construct($status = ApiResponse::STATUS_ERROR, $data = null, $error_message = null, $error_code = null) {
		$this->status	 = $status;
		$this->data		 = $data;
		$this->error	 = new ApiResponseError($error_message, $error_code);
	}

	public function setStatus($status) {
		$this->status = $status;
	}

	public function setData($data) {
		$this->data = $data;
	}

	public function setError($message, $code = null) {
		$this->status			 = self::STATUS_ERROR;
		$this->error->message	 = $message;
		$this->error->code		 = $code;
	}

	public function jsonSerialize() {
		$response = [
			'status' => $this->status
		];

		if ($this->status === self::STATUS_ERROR) {
			$response['error'] = $this->error;
			if (!empty($this->data)) {
				$response['data'] = $this->data;
			}
		} else {
			$response['data'] = $this->data;
		}

		return $response;
	}

}