<?php

final class Db {

	private $db = null;
	private static $instance = null;

	public static function GetInstance() {
		if (is_null(self::$instance)) {
			self::$instance = new self();
		}
		return self::$instance;
	}
	private function __construct(){

		if (!empty($this->db)) {
			return $this->db;
		}
		$config = Config::getConfig('db');
		try {
			$this->db = new PDO($config['dsn'], $config['username'], $config['password']);
		} catch (Exception $ex) {
			throw new Exception('DB connection error: ' . $ex->getMessage());
		}
	}

	public function __destruct() {
		$this->db = null;
	}

	public function find($data = null) {
		$result = [];
		$rows = $this->query($this->getFindSql($data));
		foreach ($rows as $row) {
			$task = new Model_Task();
			TaskMapper::map($task, $row);
			$result[$task->getId()] = $task;
		}
		return $result;
	}
	
	public function findUser($data = null) {
		return $this->query($this->getFindSql($data,false));
	}

	public function findById($id, $asArray = true) {
		$row = $this->query('SELECT * FROM task WHERE id = ' . (int) $id)->fetch();
		if (!$row) {
			return null;
		}
		if ($asArray) {
			return $row;
		}
		$task = new Model_Task();
		TaskMapper::map($task, $row);
		return $task;
	}

	public function save(Model_Task $task) {
		if (empty($task->getId())) {
			return $this->insert($task);
		}
		return $this->update($task);
	}

	private function getFindSql($data = null) {
		$sql = 'SELECT * FROM task';
		if (!empty($data)) {
			$sql .= ' WHERE 1=1 ';
			foreach ($data as $key => $val) {
				$sql .= " AND $key = '$val'";
			}
		}
		$sql .= ' ORDER BY id desc';
		return $sql;
	}

	private function insert(Model_Task $task) {
		$task->setId(null);
		$task->setStatus(Model_Task::STATUS_PENDING);
		$sql = 'INSERT INTO task (id,name,email, description, status,edited_by_admin)
                VALUES (:id,:name,:email, :description, :status,:edited_by_admin)';;
		return $this->execute($sql, $task);
	}

	private function update(Model_Task $task) {
		$sql = 'UPDATE task SET
					name = :name,
					email = :email,
					description = :description,
					status = :status,
					edited_by_admin = :edited_by_admin
				WHERE
					id = :id';
		return $this->execute($sql, $task);
	}

	private function execute($sql, Model_Task $task) {

		$statement = $this->db->prepare($sql);
		$this->executeStatement($statement, $this->getParams($task));
		if (!$task->getId()) {
			return $this->findById($this->db->lastInsertId());
		}
		return $statement->rowCount()>0;
	}

	private function getParams(Model_Task $task) {
		return [
			':id' => $task->getID(),
			':name' => $task->getUser(),
			':email' => $task->getEmail(),
			':description' => $task->getDescription(),
			':status' => $task->getStatus(),
			':edited_by_admin' => $task->getEditedByAdmin(),
		];
		
	}

	private function executeStatement(PDOStatement $statement, array $params) {

		return $statement->execute($params);
	}

	private function query($sql) {
		$statement = $this->db->query($sql, PDO::FETCH_ASSOC);
		if (!$statement) {
			self::throwDbError($this->db->errorInfo());
		}
		return $statement;
	}

	private static function throwDbError(array $errorInfo) {
		// TODO log error, send email, etc.
		throw new Exception('DB error [' . $errorInfo[0] . ', ' . $errorInfo[1] . ']: ' . $errorInfo[2]);
	}

	public function toArray($data) {
		$res = [];
		if (is_array($data)) {
			foreach ($data as $item) {
				foreach ($item as $key => $val) {
					$res[$key] = $val;
				}
			}
		}
		return $res;
	}

}
