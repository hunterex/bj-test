<?php

namespace core;

class Route {

	public static function start() {
		$controller_name = 'site';
		$action_name = 'index';

		$routes = explode('/', $_SERVER['REQUEST_URI']);
			
		if (!empty($routes[1])) {
			$controller_name = $routes[1];
		}

		if (!empty($routes[2])) {
			$action_name = $routes[2];
		}

		$model_name = 'Model_' . ucfirst($controller_name);
		$controller_name = 'Controller_' . ucfirst($controller_name);
		$action_name = 'action' . ucfirst($action_name);
		$model_path = "models/{$model_name}.php";
		if (file_exists($model_path)) {
			require_once 'config/Config.php';
			include $model_path;
			include "models/TaskMapper.php";
			include "core/Db.php";
		}

		$controller_path = "controllers/{$controller_name}.php";
		if (file_exists($controller_path)) {
			try {
				include $controller_path;
				$controller = new $controller_name;
				$action = $action_name;
				if (method_exists($controller, $action)) {
					$controller->$action();
				} else {
					Route::ErrorPage404();
				}
			} catch (Exception $ex) {

			}
		} else {
			Route::ErrorPage404();
		}
	}

	public static function ErrorPage404() {
		$host = 'http://' . $_SERVER['HTTP_HOST'] . '/';
		header('HTTP/1.1 404 Not Found');
		header("Status: 404 Not Found");
		header('Location:' . $host . '404');
	}

}
