<?php

namespace core;

class Controller {

	const BASE_URL = 'http://192.168.5.226/Test';
	
	public	$model;
	public	$view;
	public	$main;

	function __construct() {
		$this->view = new View();
	}
	
	public function redirect($url) {
		header("Location: ".self::BASE_URL."/".$url);
	}
	
	public function responseJson($res) {
		$j = json_encode($res);
		if($err =  json_last_error()){
			die($err);
		}
		header('Content-Type: application/json');
		die($j);
	}

}