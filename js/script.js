$(document).ready(function () {
	$('#tasks').DataTable({
		responsive: true,
		lengthMenu: [[3, 5, 10, 15], [3, 5, 10, 15, "All"]],
		language: {
			url: "//cdn.datatables.net/plug-ins/1.10.20/i18n/Russian.json"
		}
	});
});

unescapeHtml = function (html) {
	var txt = document.createElement("textarea");
	txt.innerHTML = html;
	return txt.value;
}

$(document).on('click', '.clicable', function () {
	let id = $(this).data('id');
	let $url = $('.js-info').data('dataurl');
	$('.text-danger').addClass('d-none')
	$('.description').text('');
	$('#task_id').text('');
	$.ajax({
		url: $url,
		method: 'post',
		data: {id: id},
		success: function (res) {
			if (res) {
				if (res.status == 1 && !res.isAdmin) {
					location = '../site/login';
				} else {
					$('.description').text(unescapeHtml(res.description));
					$('#task_id').text(res.id);
					$('.id').val(res.id);
					$('.name').val(res.name);
					$('.email').val(res.email);
					$('.status').prop('checked', res.status == 1 ? true : false);
					let mode = 'none';
					if (res.isAdmin) {
						mode = 'inline';
					}
					$('#status').css('display', mode);
					$('.modal').modal('show');
				}
			}
		}
	});
});
escapeHtml = function(text) {
	var map = {
		'&': '&amp;',
		'<': '&lt;',
		'>': '&gt;',
		'"': '&quot;',
		"'": '&#039;'
	};

	return text.replace(/[&<>"']/g, function (m) {
		return map[m];
	});
}

$(document).on('click', '.js-save', function () {
	let id = $('.id').val();
	let $url = $('.js-info').data('savedataurl');
	let email = $('.email').val();
	if (validateEmail(email) && validateRequired($('.required'))) {
		$('.email').text('');
		$.ajax({
			url: $url,
			method: 'post',
			data: {
				id: id,
				description: escapeHtml($('.description').val()),
				status: $('.status').prop('checked') ? 1 : 0,
				email: $('.email').val(),
				name: $('.name').val(),
				edited_by_admin: ''
			},
			success: function (res) {
				if (res.success) {
					$('.name').val('');
					$('.email').val('');
					$('.description').text('');
					$('#task_id').text('');
					$('.modal').modal('hide');
					alert("Данные успешно сохранены");
					location = $('.js-info').data('redirecturl');

				}
			}
		});
	}
});

function validateEmail(email) {
	let reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
	if (!reg.test(email)) {
		alert('Введите корректный e-mail');
		return false;
	}
	return true;
}

function validateRequired(obj) {
	let str = obj.val();
	let span = obj.parent().find("span");
	if (str.length === 0) {
		$(span).removeClass('d-none')
		return false;
	} else {
		$(span).addClass('d-none')
		return true;
	}
}

$(document).on('blur', '.required', function () {
	validateRequired($(this));
});