<?php

final class TaskMapper {

	public static function map(Model_Task $task, array $properties) {
		if (array_key_exists('id', $properties)) {
			$task->setId($properties['id']);
		}
		if (array_key_exists('name', $properties)) {
			$task->setUser($properties['name']);
		}
		if (array_key_exists('email', $properties)) {
			$task->setEmail($properties['email']);
		}
		if (array_key_exists('description', $properties)) {
			$task->setDescription(trim($properties['description']));
		}
		if (array_key_exists('status', $properties)) {
			$task->setStatus($properties['status']);
		}
		if (array_key_exists('edited_by_admin', $properties)) {
			$task->setEditedByAdmin($properties['edited_by_admin']);
		}
	}
}