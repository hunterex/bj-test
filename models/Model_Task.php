<?php

final class Model_Task {

	// status
	const STATUS_PENDING = 0;
	const STATUS_DONE = 1;

	private $id;
	private $name;
	private $email;
	private $description;
	private $status;
	private $edited_by_admin;

	public function __construct() {
		$this->setStatus(self::STATUS_PENDING);
	}

	public static function allStatuses() {
		return [
			self::STATUS_PENDING,
			self::STATUS_DONE,
		];
	}

	public function getId() {
		return $this->id;
	}

	public function setId($id) {
		if ($this->id !== null && $this->id != $id) {
			throw new Exception('ID уже установлен');
		}
		if ($id === null) {
			$this->id = null;
		} else {
			$this->id = (int) $id;
		}
	}

	public function getUser() {
		return $this->name;
	}

	public function setUser($name) {
		$this->name = $name;
	}
	
	public function getEmail(){
		return $this->email;
	}
	
	public function setEmail($email){
		$this->email = $email;
	}
	
	public function getDescription() {
		return $this->description;
	}

	public function setDescription($description) {
		$this->description = $description;
	}

	public function getStatus() {
		return $this->status;
	}

	public function setEditedByAdmin($user) {
		$this->edited_by_admin = $user;
	}
	
	public function getEditedByAdmin() {
		return $this->edited_by_admin;
	}

	public function setStatus($status) {
		$this->status = $status;
	}
	
	public static function getTask($id = null,$isArray = true){
		$db = Db::GetInstance();
		if(empty($id)){
			return $db->find();
		} else {
			return $db->findById($id,$isArray);
		}
	}
	public function save(){
		$db = Db::GetInstance();
		return $db->save($this);
	}

}